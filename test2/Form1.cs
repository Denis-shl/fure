﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2(this);
            this.Hide();
            newForm.Show();
            
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Form3 newForm2 = new Form3(this);
            this.Hide();
            newForm2.Show();
        }
    }
}
